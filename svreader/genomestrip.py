import logging
import sys
import os

from pysam import VariantFile

from svreader import SVRecord, SVReader, SVWriter

logger = logging.getLogger(__name__)
mydir = os.path.dirname(os.path.realpath(__file__))

'''

An ultra lightweight vcf reader for genomeSTRIP

#using awk
cat vcf |


awk  '/##FORMAT/{ last=index($0,","); id=index($0,"ID");  \
                  desc=substr($0,match($0,"Description")+12) ; \
                  gsub(/"/, "", desc); printf "%-30s %s\n", \
                  substr($0,id+3,last-id-3), substr(desc,0,length(desc)-1)}'


FILTER

    COHERENCE                      GSCOHPVALUE == NA || GSCOHPVALUE <= 0.01
    COVERAGE                       GSDEPTHCALLTHRESHOLD == NA || GSDEPTHCALLTHRESHOLD >= 1.0
    DEPTH                          GSDEPTHRATIO == NA || GSDEPTHRATIO > 0.8 || (GSDEPTHRATIO > 0.63 && (GSMEMBPVALUE == NA || GSMEMBPVALUE >= 0.01))
    DEPTHPVAL                      GSDEPTHPVALUE == NA || GSDEPTHPVALUE >= 0.01
    LQ                             Low Quality Genotyping filter
    PAIRSPERSAMPLE                 GSNPAIRS <= 1.1 * GSNSAMPLES

INFO:
    CIEND                          Confidence interval around END for imprecise variants
    CIPOS                          Confidence interval around POS for imprecise variants
    END                            End coordinate of this variant
    GSCOHERENCE                    Value of coherence statistic
    GSCOHFN                        Coherence statistic per pair
    GSCOHPVALUE                    Coherence metric (not a true p-value)
    GSCOORDS                       Original cluster coordinates
    GSCORA6                        Correlation with array intensity from Affy6 arrays
    GSCORI1M                       Correlation with array intensity from Illumina 1M arrays
    GSCORNG                        Correlation with array intensity from NimbleGen arrays
    GSDEPTHCALLS                   Samples with discrepant read pairs or low read depth
    GSDEPTHCALLTHRESHOLD           Read depth threshold (median read depth of samples with discrepant read pairs)
    GSDEPTHNOBSSAMPLES             Number of samples with discrepant read pairs in depth test
    GSDEPTHNTOTALSAMPLES           Total samples in depth test
    GSDEPTHOBSSAMPLES              Samples with discrepant read pairs in depth test
    GSDEPTHPVALUE                  Depth p-value using chi-squared test
    GSDEPTHPVALUECOUNTS            Depth test read counts (carrier inside event, carrier outside event, non-carrier inside, non-carrier outside)
    GSDEPTHRANKSUMPVALUE           Depth p-value using rank-sum test
    GSDEPTHRATIO                   Read depth ratio test
    GSDMAX                         Maximum value considered for DOpt
    GSDMIN                         Minimum value considered for DOpt
    GSDOPT                         Most likely event length
    GSDSPAN                        Inner span length of read pair cluster
    GSELENGTH                      Effective length
    GSEXPMEAN                      Expected read depth sample mean
    GSGMMWEIGHTS                   Genotyping depth model cluster weights
    GSM1                           Genotyping depth model parameter M1
    GSM2                           Genotyping depth model parameters M2[0],M2[1]
    GSMEMBNPAIRS                   Number of pairs used in membership test
    GSMEMBNSAMPLES                 Number of samples used in membership test
    GSMEMBOBSSAMPLES               Samples participating in membership test
    GSMEMBPVALUE                   Membership p-value
    GSMEMBSTATISTIC                Value of membership statistic
    GSNDEPTHCALLS                  Number of samples with discrepant read pairs or low read depth
    GSNHET                         Number of heterozygous snp genotype calls inside the event
    GSNHOM                         Number of homozygous snp genotype calls inside the event
    GSNNOCALL                      Number of snp genotype non-calls inside the event
    GSNPAIRS                       Number of discrepant read pairs
    GSNSAMPLES                     Number of samples with discrepant read pairs
    GSNSNPS                        Number of snps inside the event
    GSOUTLEFT                      Number of outlier read pairs on left
    GSOUTLIERS                     Number of outlier read pairs
    GSOUTRIGHT                     Number of outlier read pairs on right
    GSREADGROUPS                   Read groups contributing discrepant read pairs
    GSREADNAMES                    Discrepant read pair identifiers
    GSRPORIENTATION                Read pair orientation
    GSSAMPLES                      Samples contributing discrepant read pairs
    GSSNPHET                       Fraction of het snp genotype calls inside the event
    HOMLEN                         Length of base pair identical micro-homology at event breakpoints
    HOMSEQ                         Sequence of base pair identical micro-homology at event breakpoints
    IMPRECISE                      Imprecise structural variation
    NOVEL                          Indicates a novel structural variation
    SVLEN                          Difference in length between REF and ALT alleles
    SVTYPE                         Type of structural variant

FORMAT:
    CN                             Copy number genotype for imprecise events
    CNF                            Estimate of fractional copy number
    CNL                            Copy number likelihoods with no frequency prior
    CNP                            Copy number likelihoods
    CNQ                            Copy number genotype quality for imprecise events
    FT                             Per-sample genotype filter
    GL                             Genotype likelihoods with no frequency prior
    GP                             Genotype likelihoods
    GQ                             Genotype Quality
    GSPC                           Number of supporting read pairs for structural variant alleles
    GT                             Genotype
    PL                             Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification

'''


genomestrip_name = "genomestrip"
genomestrip_source = set([genomestrip_name])


class GenomeSTRIPRecord(SVRecord):

    def __init__(self, record, reference_handle=None):
        record.id = "_".join([genomestrip_name, record.chrom, record.id])
        super(GenomeSTRIPRecord, self).__init__(genomestrip_name,
                                                record.chrom,
                                                record.pos,
                                                record.stop,
                                                record.id,
                                                ref=record.ref,
                                                qual=record.qual,
                                                filter=record.filter)
        self.__record = record
        self.__sv_type = record.info['SVTYPE']
        sv = {}
        for sample in record.samples:
            sample_name = sample.rsplit('.')[0]
            sv[sample_name] = record.samples[sample]
        self.sv = sv

        variant_samples = self.variantSamples()
        if len(variant_samples) > 0:
            self.__record.info['VSAMPLES'] = ",".join(variant_samples)

    @property
    def sv_type(self):
        return self.__sv_type

    @property
    def record(self):
        return self.__record

    def addbatch2Id(self, batch=None):
        if batch:
            self.__record.id += "_" + batch

    def variantSample(self, sample):
        if self.sv[sample]["GSPC"] > 0:
            return True
        else:
            return False

    def variantSamples(self):
        variant_samples = []
        for sample in self.sv:
            if self.variantSample(sample):
                variant_samples.append(sample)
        return sorted(variant_samples)


class GenomeSTRIPReader(SVReader):
    svs_supported = set(["DEL"])

    def __init__(self, file_name, reference_handle=None, svs_to_report=None):
        super(GenomeSTRIPReader, self).__init__(file_name,
                                                genomestrip_name,
                                                reference_handle)
        self.vcf_reader = VariantFile(file_name)
        self.vcf_reader.header.info.add('VSAMPLES',
                                        number='.',
                                        type='String',
                                        description='Samples with variant alleles')
        if svs_to_report is not None:
            self.svs_supported &= set(svs_to_report)

    def __iter__(self):
        return self

    def __next__(self):
        while True:
            genomestrip_record = next(self.vcf_reader)
            record = GenomeSTRIPRecord(genomestrip_record,
                                       self.reference_handle)
            if record.sv_type in self.svs_supported:
                return record

    def getHeader(self):
        return self.vcf_reader.header

    def getOrderedSamples(self):
        samples = self.vcf_reader.header.samples
        sample_names = [sample.rsplit('.')[0] for sample in samples]
        return sample_names

    def _filter_for_gaps(sef, vcf_records, gap_regions, cutoff_proximity=50):
        """
        We do not filter for gaps for genomestrip
        """
        return vcf_records


class GenomeSTRIPWriter(SVWriter):

    def __init__(self, file_name, reference_contigs, template_reader):
        super(GenomeSTRIPWriter, self).__init__(file_name,
                                                template_reader.tool_name,
                                                template_reader)
        self.__template_reader = template_reader

    def _open(self):
        header = self.__template_reader.getHeader()
        self.vcf_writer = VariantFile(self.filename, 'w',
                                      header=header)
        self._isopen = True

    def _write(self, record):
        self.vcf_writer.write(record.record)

    def _close(self):
        if self._isopen:
            self.vcf_writer.close()
        else:  # nothing was written
            self._dumpemptyvcf()
