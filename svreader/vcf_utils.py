
import os
import vcf


def get_template_header(toolname):
    """
    Add contig infos to vcf
    :param toolname: the name of the tool
    :type toolname: string
    :return: file (path) with vcf header with toll-specific fields
    """
    thismoduledir = os.path.dirname(os.path.realpath(__file__))
    templateRessources = os.path.join(thismoduledir, "resources")
    if os.path.exists(os.path.join(templateRessources,
                                   "template_" + toolname + ".vcf")):
        template = os.path.join(templateRessources,
                                "template_" + toolname + ".vcf")
    else:
        template = os.path.join(templateRessources, "template.vcf")
    return template


def get_template(toolname):
    """
    Add contig infos to vcf
    :param toolname: the name of the tool
    :type toolname: string
    :return: a vcf.Reader object with tool specific vcf header
    """
    template = get_template_header(toolname)
    return vcf.Reader(open(template))
